__author__ = 'R. Koning, S. Konstantaras'
from time import sleep
import requests
import logging
import socket
import Queue
import copy
import sys
import os

from httpcache import CachingHTTPAdapter
from flask import Flask, Response, json

from TopologyHandler import TopologyFetcher as TF
from TopologyHandler import ddsFetcher as DF


app = Flask(__name__)

# Topology index url
ti_url = "http://localhost:5000/update"

domain_id = ""
HOST = "0.0.0.0"
PORT = 5100
tp_name = "tp"
q = Queue.Queue(1)
old_root = {}
fetcher = None

# Number of minutes to update
num_of_mins = 1

# The path to keep the log file
logging_path = os.getcwd().__str__() + "/../topoex_logs/"


def getNetworkIp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(('www.os3.nl', 0))
    return sock.getsockname()[0]


def get_nml():
    global old_root
    if q.full():
        root = q.get()
        logging.debug(tp_name + ": Found new topology in queue.")
        old_root = copy.deepcopy(root)
        return old_root
    if old_root:
        return old_root


@app.route('/topology')
def show_topology():
    root = get_nml()
    if root is not None:
        return Response(root.get_topology(), mimetype='application/xml')
    else:
        # return Response({}, mimetype='application/xml')
        return 400


@app.route('/')
def show_tp():
    url = "http://%s:%s" % (getNetworkIp(), PORT)
    new_data = dict()
    new_data[tp_name] = {'url': url, 'represents': old_root.get_name(), 'version': old_root.get_version(),
                         'topo_changed': str(fetcher.topo_changed)}
    return Response(json.dumps(new_data), mimetype='application/json'), 201


if __name__ == '__main__':

    tp_name = tp_name + "_" + str(PORT)
    dds_start = False
    if len(sys.argv) > 1:
        for arg in sys.argv:
            if arg == "-i":
                # Topology Index
                topology_index = sys.argv[sys.argv.index('-i') + 1]

            if arg == "-n":
                # Topology provider name
                tp_name = sys.argv[sys.argv.index('-n') + 1]

            if arg == "-p":
                # Topology provider port
                PORT = int(sys.argv[sys.argv.index('-p') + 1])

            if arg == "-t":
                # Topology File
                topology_file = sys.argv[sys.argv.index('-t') + 1]

            if arg == "-d":
                # The url for the DDS
                dds_url = sys.argv[sys.argv.index('-d') + 1]
                dds_start = True

            if arg == "-r":
                # Which domain is representing
                domain_id = sys.argv[sys.argv.index('-r') + 1]

            if arg == "-m":
                # Number of minutes for checking an update
                num_of_mins = int(sys.argv[sys.argv.index('-m') + 1])

            if arg == "-l":
                # The path to keep a log file
                logging_path = sys.argv[sys.argv.index('-l') + 1]
    else:
        print "Usage %s -t <topology_file> -p <port> -n <name>" % sys.argv[0]
        logging.critical("usage %s -t <topology_file> -p <port> -n <name>" % sys.argv[0])
        exit(0)

    s = requests.Session()
    s.mount('http://', CachingHTTPAdapter())
    s.mount('https://', CachingHTTPAdapter())
    logging.basicConfig(filename=logging_path + "/" + tp_name + ".log", level=logging.DEBUG)
    logging.info("%s starts at http://%s:%s" % (tp_name, HOST, PORT))
    print "%s starts at http://%s:%s" % (tp_name, HOST, PORT)

    if dds_start is False:
        # Launch reader thread
        fetcher = TF(topology_file, tp_name, HOST, PORT, ti_url, q, num_of_mins)
        fetcher.start()
        sleep(1)
        if not fetcher.is_alive():
            # print "Could not open topology file."
            logging.critical(tp_name + ": Could not open topology file.")
            exit(0)
    else:
        logging.info("%s representing '%s' with dds (%s)" % (tp_name, domain_id, dds_url))

        fetcher = DF(dds_url, tp_name, domain_id, getNetworkIp(), PORT, ti_url, q, num_of_mins)
        fetcher.start()
        sleep(1)
        if not fetcher.is_alive():
            # print "Could not open topology file."
            logging.critical(tp_name + ": Could not load the dds file.")
            exit(0)

    app.run(host=HOST, port=PORT, debug=True)

    q.task_done()
