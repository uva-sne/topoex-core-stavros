# Description #

The demo implements the architecture of the UvA/TUD topology exchange, a prototype system responsible for exchanging topology information in the NSI environment. This new system has been designed to work with multiple domains where their topology information is distributed in a hybrid approach.

Our architecture distinguishes three main components that work together to form a topology exchange and uses a simple push-pull model for distributing the network knowledge. Network administrators can deploy a Topology Provider (TP) that shares one or multiple topology files with Topology Consumers (TC) according to the currently defined policies. To discover the numerous TPs, TCs contact a central index entity called Topology Index (TI), which points to the locations of the TPs. After that, the TC will query all the corresponding domains for retrieving the latest version of their topology files and proceed to further calculations (e.g. path finding, network monitoring, etc.). Topology information inside these files is formatted according to the NML schema -which is currently supported by NSI- and a PKI can be used to aid in establishing secure paths between the entities.

As a proof of concepts we developed a demo that simulates a TI, several TPs each one representing different domain and two TCs with different roles. The first TC uses the available topology knowledge for calculating multi-domain paths while the second one monitors the network and creates a graph of the inter-connected domains. Another entity called “Lookup Service” which has access to the TI provides a mapping between available STPs and the domain where they exist.


# Directory contents #

## Libs ##
* **TopologyHandler:** A library for handling the topology files fetched from NSAs
* **nmlreader.py:** Helper library to extract information from an NML topology
* **tc.py:** Helper library to implement a topology consumer 
* **spf.py:** Helper library to implement the SPF algorithm

## Components ##
* **ti.py:** Implements a topology index 
* **tp.py:** Implements a topology provider and can use a topology file or url as source
* **tc_pf_service:** Implements a topology consumer as a service with pathfinder capabilities
* **tc_mn_service:** Implements a topology consumer as a service with monitoring finctionalities
* **ls.py:** Implements a lookup service 

## Starter ##
* **startup.py:** A script to automate the start of the simulation. It expects a certain configuration file to be passed as parameter.

## Configs ##
* **topoex-config.py:** A sample configuration file for initializing the simulation with default values

## Other ##
* **requirements:** the packages needed to run the stuff
* **README.markdown:** this file


# Usage #

Under the same directory (for example /home/user/), the user needs to place both parts of the Topology Exchange demo (core and gui). At the “topoex-config.cfg” file, the following lines MUST be configured before the initial start of the demo:
* **topoex_dir** which is the location (full path) of core part
* **log_folder** which is the location (full path) of the directory hosting the log files
* **gui_dir** which is the location (full path) of the directory containing the GUI part
* 
If the *“log_folder”* does not exist in the system, the startup script will create one at the mentioned location. In case that startup script fails to create it (e.g. due to permission errors), the user MUST create one at the mentioned location.

To initiate the simulation, the user must type the following command inside the topoex-core directory:
```
#!python

python startup.py ./topoex-config.cfg
```


If the startup script fails to initiate the GUI part, the user can activate it by typing the following command inside the coresponding folder:
```
#!python

python manage.py runserver 0.0.0.0:8000
```

It is highly recommended to initiate the GUI part with two minutes difference from the initiation of the core. At the end of the procedure the GUI can be reached by any web browser simply by typing the IP address of the machine hosting the demo.

* Topology index runs at http://localhost:5000
* Lookup service runs at http://localhost:5010
* Topology providers run at http://localhost:51xx
* Topology consumer services run at http://localhost:52xx


# Parameters #

Topology provider accepts the following parameters:
* -i: the url of the topology index
* -n: the given name of the topology provider
* -p: the TCP port
* -t: the location of the topology file (system or url)
* -d: the url of the SURFnet's dds file (disables the -t flag)
* -r: the id of the domain that TP is representing (needs -d flag)
* -m: the number of minutes that TP checks for updates of the topology
* -l: the sustem address to keep the log file

The topology consumer service accepts the following parameters:
* -i: the url of the topology index
* -l: the url of the lookup service
* -p: the TCP port
* -n: the name of the TC service
* -f: the starting STP
* -t: the ending STP
* -m: the time in minutes to consider the topology information outdated

The topology consumer instructor reads a instructions file where each line is a set of parameters:
* -u: the url of the TC service
* -p: the port of the TC service
* -f: the starting STP
* -t: the ending STP


# TODO #

* subscription of client to topology index [done by SK]
* lookup service should monitor TI and update its information [done by SK]
* proxy to topology file [RK]
* different topology views (or filtering topology views) [RK]
* input validation and proper error handling [working by SK]
* define proper interfaces [RK]
* extract neighbour info from topo index into a networkX graph (instead of using local repository) [FI]
* extract domain topology from topo index (instead of using local repository) [FI]
* link/node-disjoint path finding [FI]
* consider VLAN [FI]
* startup script to monitor the dds file and spawn new TP in case of new entry [done by SK]