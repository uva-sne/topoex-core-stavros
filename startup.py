__author__ = 'S. Konstantaras'
import io
import os
import sys
import json
import atexit
import requests
import subprocess
from time import sleep
from threading import Thread
import xml.etree.cElementTree as ET


params = {}
procs = []

topo_v2 = 'vnd.ogf.nsi.topology.v2+xml'
info_name = "/overview.json"
ti = ""
ls = ""
info = {}


def get_domains(root):
    domains = []
    for doc in list(root[1]):
        if topo_v2 in doc[1].text:
            domains.append(doc)
    return domains


def get_dds(url):

    if url.startswith('/home/'):
        with open(url, 'r') as dds_file:
            xml_root = ET.fromstring(dds_file.read())
            print "The dds file loaded"
            return xml_root
    elif url.startswith('http'):
        r = requests.get(url)
        xml_root = ET.fromstring(r.text)
        print "The dds file received"
        return xml_root
    else:
        print "The provided url is not valid"
        return


def fire_tps(domains):
    print "It's going to fire %s TPs" % len(domains)
    p = int(params.get("tp_port"))
    for d in domains:
        p += 1
        args = "-d " + dds_url + " -r " + d.get('id') + " -n " + params.get("tp_name") + \
               p.__str__() + " -p " + p.__str__() + " -i " + ti + " -m " + \
               params.get("tp_update") + " -l " + params.get("log_folder")
        try:
            cmd = "python " + topoex + "/tp.py " + args
            procs.append(subprocess.Popen([cmd], shell=True))
            info['tp' + p.__str__()] = {'name': params.get("tp_name") + p.__str__(),
                                        'url': 'http://localhost:' + p.__str__()}
            sleep(1)
        except:
            print "Failed to initiate TP for %s " % d.get('id')
            pass


def initiate_topoex(domain_ids):
    # Initiating Topology index
    print "Initiating Topology index"
    cmd = "python " + topoex + "/ti.py -p " + params.get("ti_port")
    procs.append(subprocess.Popen([cmd], shell=True))
    info['ti'] = {'url': 'http://localhost:' + params.get("ti_port").__str__()}
    sleep(1)

    # Initiating Lookup service
    print "Initiating Lookup Service"
    cmd = "python " + topoex + "/ls.py -p " + params.get("ls_port") + \
          " -u " + params.get("ls_update") + " -i " + ti + \
          " -l " + params.get("log_folder")
    procs.append(subprocess.Popen([cmd], shell=True))
    info['ls'] = {'url': 'http://localhost:' + params.get("ls_port").__str__()}
    sleep(1)

    # Spawn several topology providers
    fire_thread = Thread(target=fire_tps(domain_ids))
    fire_thread.start()

    if params.get("tc_pathfinder") is "yes" or "YES":
        # Initiating a Pathfinder Topology consumer
        tc_port = params.get("tc_pathfinder_port")
        args = "-n " + params.get("tc_pathfinder_name") + " -p " + \
               tc_port + " -i " + ti + " -l " + ls + " -m " + params.get("tc_pathfinder_time")
        print "Initiating a Topology consumer with pathfinding capabilities"
        cmd = "python " + topoex + "/tc_pf_service.py " + args
        procs.append(subprocess.Popen([cmd], shell=True))
        info['tc_' + tc_port] = {'name': params.get("tc_pathfinder_name"), 'type': "pathfinder",
                                 'url': 'http://localhost:' + params.get("tc_pathfinder_port")}
        sleep(1)

    if params.get("tc_monitoring") is "yes" or "YES":
        # Initiating a Pathfinder Topology consumer
        tc_port = params.get("tc_monitoring_port")
        args = "-n " + params.get("tc_monitoring_name") + " -p " + \
               tc_port + " -i " + ti + " -l " + ls + " -m " + params.get("tc_monitoring_time")
        print "Initiating a Topology consumer with monitoring capabilities"
        cmd = "python " + topoex + "/tc_mn_service.py " + args
        procs.append(subprocess.Popen([cmd], shell=True))
        info['tc_' + tc_port] = {'name': params.get("tc_monitoring_name"), 'type': "monitoring",
                                 'url': 'http://localhost:' + params.get("tc_monitoring_port")}

    if params.get("gui_enabled") is "yes" or "YES":
        with io.open(params.get("gui_dir") + info_name, 'w', encoding='utf-8') as outfile:
                outfile.write(unicode(json.dumps(info, outfile)))
        print "Overview file placed at " + params.get("gui_dir")
        # args = "runserver 0.0.0.0:8000"
        # cmd = "python " + params.get("gui_dir") + "/manage.py " + args
        # procs.append(subprocess.Popen([cmd], shell=True))

    return True


@atexit.register
def exit_handler():
    if len(procs) is 0:
        return
    print "Cleaning up the mess of %s processes..." % len(procs)
    for p in procs:
        if p is not None:
            p.kill()
            print "Killed one..."


if __name__ == '__main__':

    start = True
    if len(sys.argv) > 1:
        if not sys.argv[1].endswith(".cfg"):
            "Please provide a valid configuration file"
        f = open(sys.argv[1])
        line = f.readline()
        while line:
            if line and not line.startswith("#"):
                line = line.rstrip(os.linesep)
                args = line.split('=')
                params[args[0]] = args[1]
            line = f.readline()
        f.close()

        # Some basic checks of the config file
        if not params.get("dds_url"):
            print "The url of the DDS file is not present (%s)" % params.get("dds_url")
            start = False
        else:
            dds_url = params.get("dds_url")

        if not params.get("topoex_dir") or not os.path.isdir(params.get("topoex_dir")):
            print "The topology exchange folder is missing or incorrect (%s)" % params.get("topoex_dir")
            start = False
        else:
            topoex = params.get("topoex_dir")

        if not params.get("ti_url") or not params.get("ti_port"):
            print "Topology Index is not well defined."
            start = False
        else:
            ti = "http://" + params.get("ti_url") + ":" + params.get("ti_port")

        if not params.get("ls_url") or not params.get("ls_port"):
            print "Lookup Service is not well defined."
            start = False
        else:
            ls = "http://" + params.get("ls_url") + ":" + params.get("ls_port")

        if not os.path.isdir(params.get("log_folder")):
            print "Creating the log folder at %s " % params.get("log_folder")
            os.mkdir(params.get("log_folder"))

        if not start:
            exit(1)

        # Discovering the domains existing in the DDS file
        domains = []
        try:
            r = get_dds(dds_url)
            domains = get_domains(r)

        except:
            print "Failed to retrieve the domain IDs from dds: %s " % dds_url
            exit(1)

        if initiate_topoex(domains):
            print "Processes initiated: %s " % len(procs)
            while True:
                sleep(1)
    else:
        print "Please provide a configuration file."