import requests
import xml.etree.cElementTree as ET

from flask import json

from nmlreader import NMLReader


class TopologyClient:
    """
    Acts as client for topology information and an interface to the topology index,
    lookup service, and topology provider
    """

    def __init__(self, topology_index, lookup_service):
        """
        :param topology_index: the url of the topology index as a string
        :param lookup_service: the url of the lookup service as a string
        """
        self.lookup_service = lookup_service
        self.topology_index = topology_index

    def get_domain_for_stp(self, stp):
        """
        Contacts the lookup service to find the domain for the given STP
        :param stp: The Service Termination Point as a string
        :return: domain name as a string or none
        """
        data = {"stp": stp}
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(self.lookup_service + "/getdomain", json.dumps(data), headers=headers)
        if r.status_code == 200:
            j = json.loads(r.text)
            if "domain" in j.keys():
                return j["domain"]
        return None
        pass

    def get_topology_index(self):
        """
        This retrieves the full listing in the topology index
        :return: topology index object as a dictionary
        """
        r = requests.get(self.topology_index)
        index = json.loads(r.text)
        return index

    def get_topology_info(self, domain):
        """
        This retrieves summary information for the given domain name from the topology index
        :param domain: the target domain name
        :return: returns topology summary information as a dictionary
        """
        ti = self.get_topology_index()
        return ti[domain]

    def get_topology(self, domain):
        """
        contacts the index for the location of the topology for the given domain and retrieves
        it from the topology provider
        :param domain: the target domain
        :return: returns a nml topology object
        """
        ti = self.get_topology_index()
        r = requests.get(ti[domain]["topology"])
        root = ET.fromstring(r.text)
        nml = NMLReader(root)
        return nml

    def gen_ti_subscription(self, name, host, port):
        data = dict()
        data["name"] = name
        data["callback_url"] = "http://%s:%s/notifications" % (host, port)

        return data

    def subscribe_to_ti(self, data):
        """
        This method subscribes the TC to the TI in order to get
        update notifications.
        :param data: The location(url)
        :return: Boolean value
        """
        subscription = {"subscribe": data}
        try:
            url = self.topology_index + "/subscribe"
            headers = {'Content-Type': 'application/json'}
            r = requests.post(url, data=json.dumps(subscription), headers=headers)
            if r.status_code == 201:
                return True
        except:
            pass
        return False


