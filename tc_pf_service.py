__author__ = 'S. Konstantaras'
import os
import sys
import socket
import logging
import datetime
from time import sleep

from flask import request, abort
from flask import Flask, Response, json

from tc import TopologyClient
import SPF


app = Flask(__name__)

# Default values in case of none cmd parameters.
topology_index = "http://localhost:5000"
lookup_service = "http://localhost:5010"

service_name = "SNE_PF_TC"
HOST = "0.0.0.0"
PORT = 5201

begin_stp = ""
end_stp = ""
updates = 0

# Time threshold to consider a record as outdated (seconds)
time_limit = 600  # seconds
logging_path = os.getcwd().__str__() + "/../topoex_logs/"

# A dictionary containing cached information for domains of interest
# {domain : {version, pub_key, top_location, neighbors, foreign_domains, timestamp}}
domains = {}
paths = {}


def getNetworkIp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(('www.os3.nl', 0))
    return sock.getsockname()[0]


def update_domains(domain, update):
    """
     This function is responsible for processing and updating the entries for the
     domains dictionary. A Timestamp is also inserted for indicating the cached time.
     If an update is passed to the function then:
     - if the domain exists the update is inserted and True is return. Otherwise the
        update is ignored.
     If an update is not passed to the function then:
     - if the domain does not exist in the dictionary a new entry is created and a
        timestamp is inserted. Otherwise a new timestamp is inserted.
    """
    if update != "":
        if domain not in domains.keys():
            logging.critical(service_name + ": Domain %s does not exist" % domain)
            return False
        else:
            domains[update['name']] = \
                {'version': update['version'], 'topology': update['topology'],
                 'neighbors': update['neighbors'], 'foreign': update['foreign'],
                 'timestamp': datetime.datetime.now()}
            logging.debug(service_name + ": Domain %s updated." % update['name'])
            return True
    else:
        if domain not in domains.keys():
            domains[domain] = {'version': "", 'topology': "",
                               'neighbors': "", 'foreign': "",
                               'timestamp': datetime.datetime.now()}
            return True
        else:
            domains[domain]['timestamp'] = datetime.datetime.now()
            return True


def check_outdated(domain):
    tm1 = domains[domain]['timestamp']
    tm2 = datetime.datetime.now()
    if (tm2 - tm1).seconds > time_limit:
        return True
    return False


def calculate_path(from_stp, to_stp, via_domain, not_via_domain):
    f_domain = tc.get_domain_for_stp(from_stp)
    t_domain = tc.get_domain_for_stp(to_stp)

    if f_domain is None or t_domain is None:
        logging.critical(service_name + ": No domains for the given STPs")
        return

    update_domains(f_domain, "")
    update_domains(t_domain, "")

    viad = []
    if len(via_domain) > 0:
        viad.append(via_domain)

    notviad = []
    if len(not_via_domain) > 0:
        notviad.append(not_via_domain)

    if check_outdated(f_domain) or check_outdated(t_domain):
        logging.warning(service_name + ": Will calculate path with outdated information.")

    the_path = sp.fine_find_path(from_stp, f_domain, to_stp, t_domain, notviad, [], viad, [], [], [])
    paths[str(datetime.datetime.now())] = {'start_stp': from_stp, 'start_domain': f_domain, 'end_stp': to_stp,
                                               'end_domain': t_domain, 'path': the_path}
    logging.info("%s The path is %s" % (service_name, the_path))
    return the_path


@app.route('/')
def show_tc():
    global updates
    url = "http://%s:%s" % (getNetworkIp(), PORT)
    data = {'name': service_name, 'url': url, 'domains': str(len(domains.keys())), 'paths': str(len(paths.keys())),
            'time_limit': str(time_limit), 'updates': str(updates), 'type': "Pathfinder"}
    return Response(json.dumps(data), mimetype='application/json'), 201


@app.route('/domains')
def show_domains():
    return Response(json.dumps(domains), mimetype='application/json'), 201


@app.route('/paths')
def show_paths():
    return Response(json.dumps(paths), mimetype='application/json'), 201


@app.route('/notifications', methods=['POST'])
def receive_notification():
    if not request.json or not 'notification' in request.json:
        abort(400)
    info = request.json['notification']
    if 'update' in info.get('type'):
        update = info['data']
        global updates
        updates += 1
        logging.debug(service_name + " received a notification for " + update['name'])
        if not update_domains(update['name'], update):
            logging.debug(service_name + " ignores update")
    return json.dumps("Ok"), 201


@app.route('/pathfinder', methods=['POST'])
def tc_pathfinder():
    if not request.json or not 'request' in request.json:
        abort(400)

    info = request.json['request']
    logging.debug(service_name + " received pathfinding request:  %s --> %s" % (info['from'], info['to']))
    return json.dumps(calculate_path(info['from'], info['to'], info['via_domain'], info['not_domain'])), 201


if __name__ == '__main__':

    for arg in sys.argv:
        if arg == "-i":
            # Topology Index
            topology_index = sys.argv[sys.argv.index('-i') + 1]

        elif arg == "-l":
            # Lookup service
            lookup_service = sys.argv[sys.argv.index('-l') + 1]

        elif arg == "-p":
            # TCP port of the service
            PORT = int(sys.argv[sys.argv.index('-p') + 1])

        elif arg == "-n":
            # Service name
            service_name = sys.argv[sys.argv.index('-n') + 1]

        elif arg == "-f":
            # From STP
            begin_stp = sys.argv[sys.argv.index('-f') + 1]

        elif arg == "-t":
            # To STP
            end_stp = sys.argv[sys.argv.index('-t') + 1]

        elif arg == "-m":
            # Time limit
            time_limit = int(sys.argv[sys.argv.index('-m') + 1])

        elif arg == "-g":
            # Logging path
            logging_path = sys.argv[sys.argv.index('-g') + 1]


    msg = "%s starts at (http://%s:%s) for topology index ( %s ) and lookup service ( %s )\n" % \
          (service_name, HOST, PORT, topology_index, lookup_service)
    logging.basicConfig(filename=logging_path+"/"+service_name+".log", level=logging.DEBUG)
    logging.info(msg)
    print msg

    tc = TopologyClient(topology_index, lookup_service)
    sp = SPF.Pathfinder(tc)
    i = 3
    while i > 0:
        # Tries 3 times to subscribe TC to TI
        if tc.subscribe_to_ti(tc.gen_ti_subscription(service_name, getNetworkIp(), PORT)):
            logging.info(service_name + " subscribed successfully")
            i = 0
            sleep(5)
        else:
            sleep(i * 5)
            i -= 1

    app.run(debug=True, host=HOST, port=PORT)