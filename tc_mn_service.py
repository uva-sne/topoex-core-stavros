__author__ = 'S. Konstantaras'
import os
import sys
import socket
import logging
import datetime
from time import sleep

import networkx as nx
from flask import request, abort
from flask import Flask, Response, json
from networkx.readwrite import json_graph

from tc import TopologyClient

app = Flask(__name__)

# Default values in case of none cmd parameters.
topology_index = "http://localhost:5000"
lookup_service = "http://localhost:5010"
logging_path = os.getcwd().__str__() + "/../topoex_logs/"


service_name = "SNE_MN_TC"
HOST = "0.0.0.0"
PORT = 5202

updates = 0

# Time threshold to consider index as outdated (seconds)
time_limit = 600  # seconds

index = {}

# A dictionary with format {'netgraph': graph_data, 'timestamp': datetime.datetime.now() }
graph_in_json = {}


def getNetworkIp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(('www.os3.nl', 0))
    return sock.getsockname()[0]


def update_index(new_update):
    # Should either download the updated index or save the
    # new information in the existing one
    global index
    index[new_update['name']] = new_update


def is_neighbor(name):
    for item in index.iteritems:
        if name in item['neighbors']:
            return True
    return False


def get_nodes():
    nodes = []
    for item in index.keys():
        nodes.append(item)
    return nodes


def get_edges():
    edges = []
    for domain1 in index.keys():
        for domain2 in index[domain1].get('neighbors'):
            new_edge = (domain1, domain2)
            if new_edge not in edges:
                edges.append(new_edge)
    return edges


def create_graph():
    graph_obj = nx.Graph()
    graph_obj.add_nodes_from(get_nodes())
    graph_obj.add_edges_from(get_edges())
    graph_data = json_graph.node_link_data(graph_obj)
    logging.debug("%s: New network graph created at %s" % (service_name, datetime.datetime.now()))
    return graph_data


@app.route('/')
def show_tc():
    global updates
    url = "http://%s:%s" % (getNetworkIp(), PORT)
    data = {'name': service_name, 'url': url, 'time_limit': str(time_limit), 'updates': str(updates),
            'type': "Network Monitoring"}
    return Response(json.dumps(data), mimetype='application/json'), 201


@app.route('/getgraph')
def return_graph():
    global graph_in_json
    return Response(json.dumps(graph_in_json), mimetype='application/json'), 201


@app.route('/notifications', methods=['POST'])
def receive_notification():
    global updates
    global graph_in_json
    if not request.json or not 'notification' in request.json:
        abort(400)
    info = request.json['notification']
    if 'update' in info['type']:
        update = info['data']
        updates += 1
        logging.debug("%s received a notification for %s" % (service_name, update['name']))
        update_index(update)
        graph_in_json = {'netgraph': create_graph(), 'timestamp': datetime.datetime.now()}

    return json.dumps("Ok"), 201


if __name__ == '__main__':

    for arg in sys.argv:
        if arg == "-i":
            # Topology Index
            topology_index = sys.argv[sys.argv.index('-i') + 1]

        elif arg == "-l":
            # Lookup service
            lookup_service = sys.argv[sys.argv.index('-l') + 1]

        elif arg == "-p":
            # TCP port of the service
            PORT = int(sys.argv[sys.argv.index('-p') + 1])

        elif arg == "-n":
            # Service name
            service_name = sys.argv[sys.argv.index('-n') + 1]

        elif arg == "-m":
            # Time limit
            time_limit = sys.argv[sys.argv.index('-m') + 1]

        elif arg == "-g":
            # Logging path
            logging_path = sys.argv[sys.argv.index('-g') + 1]

    msg = "%s starts at (http://%s:%s) for topology index ( %s ) and lookup service ( %s )\n" % \
          (service_name, HOST, PORT, topology_index, lookup_service)
    logging.basicConfig(filename=logging_path+"/"+service_name+".log", level=logging.DEBUG)
    logging.info(msg)
    print msg

    tc = TopologyClient(topology_index, lookup_service)

    i = 3
    while i > 0:
        # Tries 3 times to subscribe TC to TI
        if tc.subscribe_to_ti(tc.gen_ti_subscription(service_name, getNetworkIp(), PORT)):
            logging.info(service_name + " subscribed successfully")
            i = 0
            sleep(5)
            index = tc.get_topology_index()
            create_graph()
        else:
            sleep(i * 5)
            i -= 1

    app.run(debug=True, host=HOST, port=PORT)