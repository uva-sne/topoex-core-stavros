'''
Exact restricted k-shortest path algorithms for weighted graphs.

Created on Jul 20, 2014
Last edited Aug 22, 2014

Authors: Farabi Iqbal <farabiiqbal@outlook.com>
        Stavros Konstantaras <s.konstantaras@uva.nl>
'''

import heapq
from collections import defaultdict

import networkx as nx

from tc import TopologyClient


class Pathfinder():
    def __init__(self, tc_obj=TopologyClient):
        self.tc_obj = tc_obj

    def spf_algorithm(self, init_list_node, s, d, K, viaN, viaE, orderN, orderE, weight='weight'):
        pk = []
        k = 0

        kpath = defaultdict(list)  # dictionary of entries maintain at domains
        for n in init_list_node.nodes():
            kpath[n] = []

        heap = []  # use heap with (distance,node,path) tuples
        kpath[s].append([s])
        heapq.heappush(heap, (0, s, kpath[s][0]))

        while heap:
            (D, u, P) = heapq.heappop(heap)
            if P in kpath[u]:
                # add requirements
                segP = []  # path by sequence of links
                for i in range(len(P) - 1):
                    segP.append((P[i], P[i + 1]))

                if u == d:
                    # check requirements
                    if set(viaN) < set(P):  # order is unimportant
                        if set(viaE) < set(segP):
                            comparen = []
                            for n in P:
                                if n in orderN:
                                    comparen.append(n)
                            if comparen == orderN:
                                comparee = []
                                for e in segP:
                                    if e in orderE:
                                        comparee.append(e)
                                if comparee == orderE:
                                    k += 1
                                    pk.append(P)
                                    if k >= K:
                                        return (pk)
                if u != d:
                    edata = iter(init_list_node[u].items())
                    for v, edgedata in edata:
                        Pv = P + [v]
                        segPv = []
                        for i in range(len(Pv) - 1):
                            segPv.append((Pv[i], Pv[i + 1]))

                        go = 1
                        # ensure no domain looping
                        for n in P:
                            if n == v:
                                go = 0

                        if go == 1:
                            # check if all remaining via is still reachable
                            H = init_list_node.copy()
                            left = list(viaN)
                            for i in range(len(Pv) - 1):
                                H.remove_node(Pv[i])
                                if Pv[i] in viaN:
                                    left.remove(Pv[i])
                            tree = nx.bfs_tree(H, Pv[-1])
                            if not (tree.has_node(d) and set(left).issubset(
                                    tree.nodes())):  # all remaining domain is still reachable
                                go = 0

                        if go == 1:
                            # ensure that ordering is fulfilled
                            compare = []
                            temp = list(orderN)
                            for n in orderN:
                                if n in Pv:
                                    compare.append(n)
                                else:
                                    temp.remove(n)
                            if compare != temp:
                                go = 0
                            compare = []
                            temp = list(orderE)
                            for e in orderE:
                                if e in segPv:
                                    compare.append(e)
                                else:
                                    temp.remove(e)
                            if compare != temp:
                                go = 0

                        if go == 1:
                            uv_dist = D + edgedata.get(weight, 1)
                            kpath[v].append(Pv)
                            heapq.heappush(heap, (uv_dist, v, Pv))
        return pk

    def fine_find_path(self, from_stp, f_domain, to_stp, t_domain,
                       notviaN=[], notviaE=[], viaN=[], viaE=[], orderN=[], orderE=[]):

        """
        viaN = [], this list contains a number of domain names that the path must travel through
        viaE = [ [], []], this list contains a number of inter-domain links that the path must travel through
        notviaN = [], this list contains a number of domain names that the path must not travel through
        notviaE = [ [], []], this list contains a number of inter-domain links that the path must not travel through
        orderN = [], this list contains a number of domain names that the path must travel through in a specific order
        orderE = [ [], []], this list contains a number of inter-domain links that the path must travel through in a specific order
        """

        # check if both domains are equal
        if f_domain == t_domain:
            path = list()
            path.append([from_stp])
            path.append([to_stp])
            return path

        # create a multi-domain network representation
        graph = self.tc_obj.get_topology_index()
        G = nx.Graph()
        for domain in graph:
            G.add_node(domain)
            for x in graph[domain]:
                if x == "neighbors":
                    for neighbor in graph[domain][x]:
                        G.add_node(neighbor)
                        G.add_edge(domain, neighbor, weight=1)

        # number of considered inter-domain paths
        K = 1

        # requirement relation
        viaN = list(set(viaN + orderN))
        viaE = list(set(viaE + orderE))

        # remove forbidden nodes and links
        if len(notviaN) > 0:
            for n in notviaN:
                G.remove_node(n)
        if len(notviaE) > 0:
            for (u, v) in notviaE:
                G.remove_edge(u, v)

        # selective path finding
        new_path = self.spf_algorithm(G, f_domain, t_domain, K, viaN, viaE, orderN, orderE)

        # refine the first (and only) inter-domain path
        path = list()
        j = 0
        path.append([from_stp])
        while j < len(new_path[0]) - 1:
            f_topology = self.tc_obj.get_topology(new_path[0][j])
            for f_sdp in f_topology.get_sdps():
                if str(new_path[0][j]) in f_sdp[0]:
                    if str(new_path[0][j + 1]) in f_sdp[1]:
                        path.append([f_sdp[0], f_sdp[1]])
                        break
            j += 1
        path.append([to_stp])
        return path