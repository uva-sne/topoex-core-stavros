__author__ = 'R. Koning, S. Konstantaras'
import os
import sys
import socket
import logging
import requests
import datetime
from threading import Thread

from flask import Flask, Response, json
from flask import request, abort


app = Flask(__name__)

HOST = "0.0.0.0"
PORT = 5000
updates = 0

index = {}
subscribers = {}
# It has the following format:
# {'name': {'date': timestamp, 'num_of_updates': integer}}
last_updates = {}

logging_path = os.getcwd().__str__() + "/../topoex_logs/"


def getNetworkIp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(('www.os3.nl', 0))
    return sock.getsockname()[0]


def notify_all_tc(data):
    """
    This function is responsible to notify all subscribed TCs
    for a new topology update provided by any TP.
    """
    headers = {'Content-Type': 'application/json'}
    for tc_name in subscribers.keys():
        try:
            url = subscribers.get(tc_name).get('address')
            r = requests.post(url, data=data, headers=headers)
            if r.status_code == 201:
                logging.debug("TI: notified successfully %s " % tc_name)
        except:
            logging.warning("TI: failed to notify %s " % tc_name)
            pass


@app.route('/')
def show_ti():
    return Response(json.dumps(index), mimetype='application/json'), 201


@app.route('/info')
def show_ti_info():
    info = {'url': "http://" + getNetworkIp() + ":" + str(PORT), 'index_size': str(len(index.keys())),
            'sub_size': str(len(subscribers.keys())), 'updates': updates}
    return Response(json.dumps(info), mimetype='application/json'), 201


@app.route('/update', methods=['POST'])
def update_ti():
    global notify_thread
    global updates
    if not request.json or not 'update' in request.json:
        abort(400)
    tp_info = request.json['update']
    index[tp_info['name']] = tp_info
    logging.debug("\nTI: received update for %s (version: %s )\n" % (tp_info['name'], tp_info['version']))
    updates += 1

    tp_info['neighbors'] = []
    tp_info['foreign'] = []

    for peer in tp_info['peers']:
        if peer in index.keys():
            tp_info['neighbors'].append(peer)
        else:
            tp_info['foreign'].append(peer)
        print ""
    tp_info['foreign']
    del tp_info['peers']

    for nb in tp_info['neighbors']:
        peer = tp_info['name']
        changed_peers = list()
        if peer in index[nb]['foreign']:
            index[nb]['neighbors'].append(peer)
            changed_peers.append(peer)
        for peer in changed_peers:
            index[nb]['foreign'].remove(peer)

    if len(subscribers) > 0:
        # Create a new thread to notify TCs
        nt = {'notification': {'type': "update", 'data': tp_info}}
        notify_thread = Thread(target=notify_all_tc(json.dumps(nt)))
        notify_thread.start()

    if tp_info['name'] in last_updates:
        upds = int(last_updates[tp_info['name']].get('updates'))
        last_updates[tp_info['name']] = {'last_update': datetime.datetime.now(), 'updates': str(upds + 1)}
    else:
        last_updates[tp_info['name']] = {'last_update': datetime.datetime.now(), 'updates': str(1)}
        # print last_updated

    return json.dumps(tp_info), 201


@app.route('/subscribe', methods=['POST'])
def subscribe_ti():
    """
    This function receives the subscription request from a TC
    and saves its link into subscribers list.
    """
    if not request.json or not 'subscribe' in request.json:
        abort(400)

    sb_info = request.json['subscribe']
    if sb_info['name'] not in subscribers.keys():
        subscribers[sb_info['name']] = {'address': sb_info['callback_url'], 'timestamp': str(datetime.datetime.now())}
        logging.debug("\nTI: %s subscribed with callback address (%s)" % (sb_info['name'], sb_info['callback_url']))

    return json.dumps('OK'), 201


@app.route('/subscribers', methods=['GET'])
def get_subscribers():
    return Response(json.dumps(subscribers), mimetype='application/json'), 201


@app.route('/updates_info', methods=['GET'])
def show_updates_info():
    return Response(json.dumps(last_updates), mimetype='application/json'), 201


if __name__ == '__main__':

    for arg in sys.argv:
        if arg == "-p":
            # TCP port of the service
            PORT = int(sys.argv[sys.argv.index('-p') + 1])

        elif arg == "-l":
            logging_path = sys.argv[sys.argv.index('-l') + 1]

    logging.basicConfig(filename=logging_path + "/ti.log", level=logging.DEBUG)
    logging.info("TI starts at (%s) with address: (http://%s:%s)\n" % (datetime.datetime.now(), HOST, PORT))
    print ("TI starts at (%s) with address: (http://%s:%s)\n" % (datetime.datetime.now(), HOST, PORT))
    app.run(host=HOST, port=PORT, debug=True)
