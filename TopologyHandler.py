__author__ = 'R. Koning, S. Konstantaras'
import xml.etree.cElementTree as ET
from time import sleep
import threading
import requests
import logging
import os

from flask import json

from nmlreader import NMLReader


def gen_ti_upd(root, host, port):
    """
    The following function generates a topology update for TI
    in a python dictionary and returns it back to the caller.
    """
    output = dict()
    output["name"] = root.get_name()
    output["version"] = root.get_version()
    output["peers"] = root.get_peers()
    output["topology"] = "http://%s:%s/topology" % (host, port)
    return output


def post_to_ti(ti_url, tp_name, data):
    """
    The following function converts the update into json format
    and sends it to the TI by HTTP Post.
    """
    try:
        headers = {'Content-Type': 'application/json'}
        r = requests.post(ti_url, data=json.dumps(data), headers=headers)
        if r.status_code == 201:
            logging.debug(tp_name + ": updated TI successfully")
            return True
    except:
        pass
    return False


class TopologyFetcher(threading.Thread):
    def __init__(self, topo_file, name, host, port, url, q, mins):
        self.ti_url = url
        self.q = q
        self.topology_file = topo_file
        self.topo_changed = 0
        self.tp_name = name
        self.HOST = host
        self.PORT = port
        self.num_of_mins = mins
        threading.Thread.daemon = True
        threading.Thread.__init__(self)

    def run(self):
        version = None
        last = None
        data = {"update": {"name": "test"}}
        update_ti = False

        if self.topology_file is None or not os.path.exists(self.topology_file) and not self.topology_file.startswith(
                "http"):
            logging.critical(self.tp_name + ": No topology found at: " + self.topology_file)
            exit(1)

        while True:
            # That is the main loop with all the critical functionality
            if self.topology_file.startswith("http"):
                """
                This block sends a request to the server, receives the
                topology file and keeps the content in a variable
                """
                logging.debug(self.tp_name + ": Receiving topology file from http server...")
                r = requests.get(self.topology_file)
                root = ET.fromstring(r.text)
                nml = NMLReader(root)

            elif os.stat(self.topology_file).st_mtime > last:
                """
                This block reads the timestamp of the topology file and
                in case of more recent one keeps the last version
                """
                logging.debug(self.tp_name + ": Detected topology file change...")
                last = os.stat(self.topology_file).st_mtime
                root = ET.parse(self.topology_file).getroot()
                nml = NMLReader(root)

            else:
                sleep(5)
                continue

            newversion = nml.get_version()
            if version != newversion:
                logging.debug(self.tp_name + ": New topology document loaded, old: %s, new: %s" % (version, newversion))
                version = newversion
                self.topo_changed += 1
                data = {"update": gen_ti_upd(nml, self.HOST, self.PORT)}
                update_ti = True
                if self.q.full():
                    self.q.get()
                self.q.put(nml, False)

            if update_ti is True:
                logging.debug(self.tp_name + ": Updating TI...")
                if post_to_ti(self.ti_url, self.tp_name, data):
                    update_ti = False
                else:
                    update_ti = True
                    logging.warning(self.tp_name + ": TI update failed")
            if self.topology_file.startswith("http"):
                logging.debug(self.tp_name + ": Source is website, waiting %s min for update" % self.num_of_mins)
                sleep(self.num_of_mins * 60 + 55)
            sleep(5)


class ddsFetcher(threading.Thread):
    def __init__(self, dds, name, domain, host, port, url, q, mins):
        self.ti_url = url
        self.q = q
        self.dds_url = dds
        self.tp_name = name
        self.tp_domain = domain
        self.topo_changed = 0
        self.HOST = host
        self.PORT = port
        self.num_of_mins = mins
        threading.Thread.daemon = True
        threading.Thread.__init__(self)

    def run(self):
        version = None
        data = {"update": {"name": "test"}}
        update_ti = False

        while True:
            croot = self.get_root()  # get the corresponding root of the domain in the dds file
            if croot is not None:
                nml = NMLReader(croot)
                newversion = nml.get_version()
                logging.debug(self.tp_name + ": %s has a file with version %s and peers: %s" % (
                    nml.get_name(), newversion, nml.get_peers()))
                if version != newversion:
                    self.topo_changed += 1
                    logging.debug(
                        self.tp_name + ": New topology document loaded, old: %s, new: %s" % (version, newversion))
                    version = newversion
                    data = {"update": gen_ti_upd(nml, self.HOST, self.PORT)}
                    update_ti = True
                    if self.q.full():
                        self.q.get()
                    self.q.put(nml, False)

                if update_ti is True:
                    logging.debug(self.tp_name + ": Updating TI...")
                    if post_to_ti(self.ti_url, self.tp_name, data):
                        update_ti = False
                    else:
                        update_ti = True
                        logging.warning(self.tp_name + ": TI update failed")
            else:
                logging.warning("%s: Failed to read the DDS file for domain %s" % (self.tp_name, self.tp_domain))

            logging.debug("%s: waiting %s min for update" % (self.tp_name, self.num_of_mins))
            sleep(self.num_of_mins * 60)

    def get_root(self):
        """
        This function retrieves the DDS file from the provider and discovers
        the corresponding nml file inside it.
        :return: A pointer in the dds file of the current domain or None
        """
        if self.dds_url is None or self.tp_domain is None:
            logging.critical(self.tp_domain + ": No topology found at: " + self.dds_url)
            exit(1)
        elif self.dds_url.startswith('/home/'):
            with open(self.dds_url, 'r') as dds_file:
                xml_root = ET.fromstring(dds_file.read())
                logging.debug(self.tp_name + ": The dds file loaded")
                docs = xml_root[1]
                for doc in docs:
                    if self.tp_domain in doc.get('id'):
                        return doc
                else:
                    logging.debug("Not Found %s in dds file" % self.tp_domain)
                    return
        else:
            r = requests.get(self.dds_url)
            if r.status_code == 200:
                xml_root = ET.fromstring(r.text)
                logging.debug(self.tp_name + ": The dds file received")
                docs = xml_root[1]
                for doc in docs:
                    if self.tp_domain == doc.get('id'):
                        return doc
            else:
                return None


