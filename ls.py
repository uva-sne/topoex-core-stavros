__author__ = 'S. Konstantaras, R. Koning'
import os
import sys
import socket
import logging
import requests
import threading
from time import sleep
import xml.etree.cElementTree as ET

from flask import Flask, Response, json, abort, request

from nmlreader import NMLReader


app = Flask(__name__)

HOST = "0.0.0.0"
PORT = 5010
topology_index = "http://localhost:5000"

stp_domain_map = {}
index = {}

update_interval = 180  # seconds
updates = 0
# The path to keep the log file
logging_path = os.getcwd().__str__() + "/../topoex_logs/"


def getNetworkIp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(('www.os3.nl', 0))
    return sock.getsockname()[0]


def get_stp_list(topology):
    i = 3
    r = requests.Response
    while i > 0:
        try:
            r = requests.get(topology)
            i = 0
        except:
            i -= 1
            logging.warning("LS: Error contacting %s, sleeping for %s seconds." % (topology, i))
            sleep(i)
            pass
    if r.status_code is not 200:
        logging.warning("LS: Received nothing from %s ( %s )" % (topology, r.status_code))
        return None
    root = ET.fromstring(r.text)
    nmlobj = NMLReader(root)
    loc_stp_list = nmlobj.get_stps()
    return loc_stp_list


def populate_domain_map():
    for domain, attr in index.items():
        stp_list = get_stp_list(attr['topology'])
        if stp_list is None:
            return False
        for stp in stp_list:
            stp_domain_map[stp] = domain


def get_ti_index():
    try:
        r = requests.get(topology_index)
    except:
        return False
        pass
    if r.status_code == 201:
        global index
        index = json.loads(r.text)
        return True
    logging.warning("LS: Failed to contact TI (%s)" % r.status_code)
    return False


def update_all_ls():
    if get_ti_index() and len(index.items()) > 0:
        if not populate_domain_map():
            return False
        logging.debug("LS: Re-initialised")


def update_ls(update):
    global stp_domain_map
    topo = update['topology']
    stp_list = get_stp_list(topo)
    if stp_list is not None:
        for stp in stp_list:
            stp_domain_map[stp] = update['name']


def subscribe_to_ti(host, port):
    """
    This method subscribes the LS to the TI in order to get
    update notifications.
    """

    data = dict()
    data["name"] = "LS"
    data["callback_url"] = "http://%s:%s/notifications" % (host, port)
    subscription = {"subscribe": data}
    try:
        url = topology_index + "/subscribe"
        headers = {'Content-Type': 'application/json'}
        r = requests.post(url, data=json.dumps(subscription), headers=headers)
        if r.status_code == 201:
            return True
    except:
        pass
    return False


@app.route('/getdomain', methods=['POST'])
def get_domain():
    if not request.json or not 'stp' in request.json:
        abort(400)
    logging.debug("LS: received request for stp %s " % request.json['stp'])
    stp = request.json['stp']
    domain = None
    try:
        domain = stp_domain_map[stp]
        # print "LS: The domain for the given STP is: " + domain
    except:
        abort(404)
    result = json.jsonify({"domain": domain})
    return result


@app.route('/notifications', methods=['POST'])
def receive_notification():
    if not request.json or not 'notification' in request.json:
        logging.debug("LS: Message is not a notification")
        abort(400)
    info = request.json['notification']
    if 'update' in info['type']:
        global updates
        updates += 1
        update = info['data']
        logging.debug("LS: received a notification for %s" % update['name'])
        init_thread = threading.Thread(target=update_ls(update))
        init_thread.start()
    return Response(), 201


@app.route('/')
def get_stps():
    result = json.jsonify({"stps": stp_domain_map})
    return result


@app.route('/info')
def get_info():
    global updates
    url = "http://%s:%s" % (getNetworkIp(), PORT)
    data = {'url': url, 'stps': str(len(stp_domain_map.keys())), 'upd_interval': update_interval, 'updates': updates}
    result = json.jsonify(data)
    return result, 201


if __name__ == '__main__':

    for arg in sys.argv:
        if arg == "-i":
            # Topology Index
            topology_index = sys.argv[sys.argv.index('-i') + 1]

        elif arg == "-u":
            # The value of the update interval
            update_interval = int(sys.argv[sys.argv.index('-u') + 1])

        elif arg == "-p":
            # TCP port of the service
            PORT = int(sys.argv[sys.argv.index('-p') + 1])

        elif arg == "-l":
            # Where to keep the log file
            logging_path = sys.argv[sys.argv.index('-l') + 1]

    logging.basicConfig(filename=logging_path + "/ls.log", level=logging.DEBUG)
    i = 3
    while i > 0:
        try:
            if subscribe_to_ti(getNetworkIp(), PORT):
                i = 0
                threading.Timer(update_interval, update_all_ls).start()
                print "LS: initialised successfully with update interval of %s seconds" % update_interval
                logging.info("LS: initialised successfully with update interval of %s seconds" % update_interval)

        except:
            i -= 1
            logging.critical("LS: Connection to TI failed. Sleeping for %s seconds\n" % str(i * 5))
            sleep(i * 5)
            pass

    app.run(host=HOST, port=PORT, debug=True)
