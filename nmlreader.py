__author__ = 'R. Koning, S. Konstantaras'
import xml.etree.cElementTree as ET


class NMLReader():
    def __init__(self, xml_root):
        self.nml_ns = "http://schemas.ogf.org/nml/2013/05/base#"
        ET.register_namespace('nml', self.nml_ns)
        self.root = xml_root

    def get_peers(self):
        """ Returns all peers of the domain"""
        aliases = self.root.findall(".//*[@type='" + self.nml_ns + "isAlias']")
        networks = []
        for child in aliases:
            for c2 in child:
                try:
                    name = c2.attrib['id']
                    name = self._strip_prefix(name, "urn:ogf:network:").rsplit(":")[0]
                    if name not in networks:
                        networks.append(name)
                except:
                    pass
        return networks

    def get_stps(self):
        """ Returns all stps of the domain"""
        topo = self.root[2][0]
        stps = []
        aliases = topo.findall("{" + self.nml_ns + "}" + "BidirectionalPort")
        # aliases = self.root.findall("{" + self.nml_ns + "}" + "BidirectionalPort")
        for al in aliases:
            name = al.attrib['id']
            stps.append(name)
        return stps

    def get_sdps(self):
        """ Returns all sdps of the domain"""
        sdp = list()
        sdp_port = self.root.findall(".//*[@type='" + self.nml_ns + "isAlias']/..")
        for port in sdp_port:
            rel = port.find("{" + self.nml_ns + "}" + "Relation")
            remote = rel.find("{" + self.nml_ns + "}" + "PortGroup")
            local_port = port.attrib['id']
            remote_port = remote.attrib['id']
            sdp.append([local_port, remote_port])
        return sdp

    def get_parentport(self, stp):
        """ Returns all sdps of the domain"""
        aliases = self.root.findall("{" + self.nml_ns + "}" + "BidirectionalPort")
        for child in aliases:
            groups = child.findall("{" + self.nml_ns + "}" + "PortGroup")
            for group in groups:
                name = group.attrib['id']
                if name == stp:
                    return child.attrib['id']
        return None

    def get_subports(self, stp):
        """ Returns all sdps of the domain"""
        aliases = self.root.findall("{" + self.nml_ns + "}" + "BidirectionalPort")
        stps = []
        for child in aliases:
            if child.attrib['id'] == stp:
                groups = child.findall("{" + self.nml_ns + "}" + "PortGroup")
                for group in groups:
                    name = group.attrib['id']
                    stps.append(name)
        return stps

    def get_topology(self):
        """ Returns string with the full topology"""
        return ET.tostring(self.root, encoding='utf8', method='xml')

    def get_version(self):
        """ Returns version of NML document"""
        return str(self.root.attrib['version'])

    def get_name(self):
        """ Returns name of the domain"""
        return self._strip_prefix(self.root.attrib['id'], "urn:ogf:network:").rsplit(":")[0]

    def _strip_prefix(self, text, prefix):
        assert text.startswith(prefix), 'Text did not start with specified prefix (text: %s, prefix: %s)' % (
            text, prefix)
        ul = len(prefix)
        return text[ul:]

    def __str__(self):
        return self.get_topology()


